pokemon?limit=50&offset=0
name = results[0].name

FETCH /pokemon/<name>/

arrays POKEMONS {
[
id -> https://pokeapi.co/api/v2/pokemon/<id>/
sprites -> images
types -> https://github.com/duiker101/pokemon-type-svg-icons
weight
height
stats
],
}

FETCH /pokemon-species/<name>/
if (.evolves_from_species:null) -> .evolution: false
