console.log('run');

let lastIndex = 0;
const totalPokemons = 151;
let pokemonsToLoad = 10;
const pokemonsLoadeds = [];
const pokemonsList$$ = document.getElementById('pokemonsList');

const callApi = async (lastIndex, pokemonsToLoad) => {
    const currentIndex = lastIndex;
    for (let index = currentIndex; index < pokemonsToLoad + currentIndex; index++) {
        try {
                const dateApi = await fetch(`https://pokeapi.co/api/v2/pokemon/${lastIndex +1}/`);
                const dateApiJSON = await dateApi.json();
                pokemonsLoadeds[lastIndex] = dateApiJSON;
                lastIndex++;
                // console.log(lastIndex);
        } catch (error) {
            console.error('error', error);
        }

    }
    return {lastIndex, currentIndex};

};

const preLoadPaint = (pokemonsToLoad) => {
    const newlist = [];
    for (let i = 0; i < pokemonsToLoad; i++) {
        const item$$ = document.createElement('article');
        item$$.className = `pokemons__item`;
        item$$.innerHTML = `
        <div class="pokemons__aside">
        <img width="200" src="/assets/images/pokeball.svg" alt="" class="pokemons__image" loading="lazy">
        <span class="pokemons__id"> </span>
        </div>
        <div class="pokemons__body">
            <h2 class="pokemons__title"> </h2>
            <div class="pokemons__types types">
            </div>
            <div class="pokemons__physical physical">
                <div class="physical__warp">
                    <p class="physical__value--weight"> </p>
                    <p class="physical__name">Weight</p>
                </div>
                <div class="physical__warp">
                    <p class="physical__value--height"> </p>
                    <p class="physical__name">Height</p>
                </div>
            </div>
        </div>
        `;
        pokemonsList$$.appendChild(item$$);
        newlist.push(item$$);
    }
    return newlist;
}

const afterLoadPaint = (pokemonsToLoadIndexRange, newlist) => {
    let indexNodes = 0;
    for (let index = pokemonsToLoadIndexRange.currentIndex; index < pokemonsToLoadIndexRange.lastIndex; index++) {

        const item$$ = newlist[indexNodes];

        const pokemon = pokemonsLoadeds[index];
        let {id, name, weight, height, types, sprites } = pokemon;
        const image = sprites.other.dream_world.front_default;

        item$$.classList.add(`${types[0].type.name}`);

        const image$$ = item$$.querySelector('img');
        image$$.setAttribute('src', image);
        image$$.setAttribute('alt', name);

        item$$.querySelector('.pokemons__id').textContent = `#${id}`;

        item$$.querySelector('h2').textContent = name;

        const types$$ = item$$.querySelector('.types');
        for (const type of types) {
            const type$$ = document.createElement('span');
            type$$.className = `types__item ${type.type.name}`;
            type$$.textContent = type.type.name;
            types$$.appendChild(type$$);
        }

        weight = `${weight / 10} kg`;
        height = `${height / 10} m`;
        item$$.querySelector('.physical__value--weight').textContent = weight;
        item$$.querySelector('.physical__value--height').textContent = height;

    indexNodes++;
    }
}

const paintDateApiJSON = async (lastIndex, pokemonsToLoad) => {
    
    const newlist = preLoadPaint(pokemonsToLoad);
    
    const pokemonsToLoadIndexRange = await callApi(lastIndex, pokemonsToLoad);
    afterLoadPaint(pokemonsToLoadIndexRange, newlist);

}


const times = Math.trunc(totalPokemons / pokemonsToLoad);
const spare = totalPokemons % pokemonsToLoad;
let oneMoreTime = true;
let index = 0;

const remainingPokemons = () => {
    const copyPokemonsToLoad = pokemonsToLoad;
    pokemonsToLoad = spare;
    oneMoreTime = false;
    lastIndex += copyPokemonsToLoad;
    deferLoadPokemon(index);
} 

const deferLoadPokemon = async () => {
        await paintDateApiJSON(lastIndex, pokemonsToLoad);
        index++;
    if( index < times) {
        lastIndex += pokemonsToLoad;
        deferLoadPokemon(index);
    } else if (spare != 0 && oneMoreTime) {
        remainingPokemons();
    }
}

deferLoadPokemon();


/**
 * 
 * EXPORT
 * 
 * 
 */

  export {totalPokemons};